<?php

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
     /**
     * @var array
     */
    protected $settings = [
        [
            'key'                       =>  'site_name',
            'value'                     =>  'Hydrophonix',
        ],
        [
            'key'                       =>  'site_title',
            'value'                     =>  'Hydrophonix Kenya',
        ],
        [
            'key'                       =>  'default_email_address',
            'value'                     =>  'admin@hydrophonix.com',
        ],
        [
            'key'                       =>  'currency_code',
            'value'                     =>  'KES',
        ],
        [
            'key'                       =>  'currency_symbol',
            'value'                     =>  '/=',
        ],
        [
            'key'                       =>  'site_logo',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'site_favicon',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'footer_copyright_text',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'seo_meta_title',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'seo_meta_description',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'social_facebook',
            'value'                     =>  'https://facebook.com/hydrophonix',
        ],
        [
            'key'                       =>  'social_twitter',
            'value'                     =>  'https://twitter.com/hydrophonix',
        ],
        [
            'key'                       =>  'social_instagram',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'social_linkedin',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'google_analytics',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'facebook_pixels',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'stripe_payment_method',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'stripe_key',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'stripe_secret_key',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'paypal_payment_method',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'paypal_client_id',
            'value'                     =>  'AcTnOxjbR-_QYRvV0MnRAVCZytWfCkTVrxelLu-HYZpr3UMoyuDgcUappvABoTV9KedMkdf45kWTbJCw',
        ],
        [
            'key'                       =>  'paypal_secret_id',
            'value'                     =>  'EF7foCMUl4lFM6asCgGxO9vOdognwBKzSJnwgCUq8Xb7NMylWu6ImYY1tA4jCTuFXtbZ8hVuqngOOOF1',
        ],
        [
            'key'                       =>  'mpesa_payment_method',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'mpesa_client_id',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'mpesa_secret_id',
            'value'                     =>  '',
        ],
    ];
    


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     foreach ($this->settings as $index=>$setting) 
     {
          $result = Setting::create($setting);

          if (!$result) {
               $this->command->info('Insert failed at record $index.');
               return;
          }
     }

     $this->command->info('Inserted '. count($this->settings). 'records');
    }
}
